//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.FileNotFoundException;
import java.util.ArrayList;

class MoveMain {

   public static void main(String[] args) throws FileNotFoundException {
      ArrayList<String> answers;
      int maxRecords;
      switch(args.length) {
         case 0:
            answers = Ask.getAnswers(null);
            maxRecords = Ask.forReal();

            Setup.readTranslateTables(answers.size()>=6 ? answers.get(5) : null);
            Record.setCommands(Setup.readCommands(answers.get(1)));
            System.out.println("Initialization Complete");
            if (Verify.verifyFormatFile() && Verify.verifyTranslateTables()) {
               Processor.processRecords(answers, maxRecords);
            }
            break;
         case 1:
            if (args[0].equals("SE")) {
               answers = Ask.getSEAnswers(null);
               maxRecords = Ask.forReal();
               System.out.println("SE");
               System.out.println("C:\\JAKE\\TEST\\jakeans.txt");
               if ((answers.get(14)).equals(" ")) {
                  answers.set(14, answers.get(9));
               }

               Setup.readTranslateTables(answers.get(13));
               Record.setCommands(Setup.readCommands(answers.get(8)));
               System.out.println("Initialization Complate");
               if (Verify.verifyFormatFile() && Verify.verifyTranslateTables()) {
                  Processor.processSERecords(answers, maxRecords);
               }
            } else if (args[0].equals("MO")) {
               answers = Ask.getAnswers(null);
               maxRecords = Ask.forReal();
               if ((answers.get(6)).equals(" ")) {
                  answers.set(6, answers.get(3));
               }

               Setup.readTranslateTables(answers.size()>=6 ? answers.get(5) : null);
               Record.setCommands(Setup.readCommands(answers.get(1)));
               System.out.println("Initialization Complete");
               if (Verify.verifyFormatFile() && Verify.verifyTranslateTables()) {
                 Processor.processRecords(answers, maxRecords);
               }
            }
            break;
         case 2:
            if (args[0].equals("SE")) {
               answers = Ask.getSEAnswers(args[1]);
               maxRecords = Ask.forReal();
               System.out.println("SE");
               System.out.println("C:\\JAKE\\TEST\\jakeans.txt");
               if ((answers.get(14)).equals(" ")) {
                  answers.set(14, answers.get(9));
               }

               Setup.readTranslateTables(answers.get(13));
               Record.setCommands(Setup.readCommands(answers.get(8)));
               System.out.println("Initialization Complate");
               if (Verify.verifyFormatFile() && Verify.verifyTranslateTables()) {
                  Processor.processSERecords(answers, maxRecords);
               }
            } else if (args[0].equalsIgnoreCase("MO")) {
               answers = Ask.getAnswers(args[1]);
               Setup.readTranslateTables(answers.size()>=6 ? answers.get(5) : null);
               Record.setCommands(Setup.readCommands(answers.get(1)));
               System.out.println("Initialization Complete");
               if (Verify.verifyFormatFile() && Verify.verifyTranslateTables()) {
                  Processor.processRecords(answers, -1);
               }
            }
      }

      System.out.println("The Program is Done doing things");
   }
}
