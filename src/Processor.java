import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

class Processor {

   static void processRecords(ArrayList<String> answers, int maxRecords) {
      File inFile = new File(answers.get(0));
      File outFile = new File(answers.get(2));
      int iCardCount = Integer.parseInt(answers.get(3));
      int oCardCount = Integer.parseInt(answers.get(4));
      File cFile = null;
              if(answers.size() >=7) {
                 if(!answers.get(5).equalsIgnoreCase(TranslateTable.getFileName())){
                    cFile = new File(answers.get(5));
                 }

              }

      try {
         if (Record.getCommands().size() == 0)
            return;
         int offset = 0;
         int update = 10;
         int iLineLength = 120;
         int oLineLength = 120;
         if (Record.getCommands().get(0)[0].equals("FA")) {
            iLineLength = Integer.parseInt(Record.getCommands().get(0)[1]);
            oLineLength = Integer.parseInt(Record.getCommands().get(0)[2]);
            Record.getCommands().remove(0);
            offset++;
         }
         for (int x = 0; x < Record.getCommands().size(); x++) {
            if (Record.getCommands().get(x)[0].equals("MC")) {
               update = Integer.parseInt(Record.getCommands().get(x)[1]);
               Record.getCommands().remove(x);
               offset++;
            }
         }
         if (!Verify.verifyInput(iCardCount, oCardCount, iLineLength, oLineLength, offset))
            return;
         int recordCounter = 0;
         PrintWriter writer = new PrintWriter(outFile);
         Scanner reader = new Scanner(new FileReader(inFile));
         PrintWriter cWriter = cFile != null ? new PrintWriter(cFile): null;

         while (reader.hasNext()) {
            ArrayList<String> input = new ArrayList<>();
            for (int c = 0; c < iCardCount; c++) {
               String line = reader.nextLine();
               input.add(String.format("%-" + iLineLength + "." + iLineLength + "s", line));
            }
            Record IORecord = new Record(input, oCardCount, oLineLength);
            IORecord.runCommands();
            if (!IORecord.getClosed()) {
               for (int x = 0; x < IORecord.getOutput().size(); x++) {
                  if (maxRecords < 0) {
                     writer.println(IORecord.getOutput().get(x));
                  } else {
                     System.out.println(IORecord.getOutput().get(x));
                  }
               }
            }
            if (IORecord.getClosed()) {
               if (IORecord.getWriteOutput()) {
                  for (String value : IORecord.getOutput()) {
                     if (maxRecords < 0) {
                        if (lineContainsClose(value))
                           if (cWriter!= null)
                              cWriter.println(value);
                        else
                           writer.println(value);
                     } else {
                        System.out.println(value);
                     }
                  }
               } else {
                  for (int x = 0; x < IORecord.getInput().size(); x++) {
                     if (maxRecords < 0) {
                        if (cWriter!= null)
                           cWriter.println(IORecord.getInput().get(x));
                     } else {
                        System.out.println(IORecord.getInput().get(x));
                     }
                  }
               }
            }
            recordCounter++;
            if (recordCounter % update == 0) {
               System.out.println("Successfully finished " + recordCounter + " records.");
               if (recordCounter == maxRecords) {
                  return;
               }
            }

         }

         writer.close();
         reader.close();
      } catch (IOException ignored) {}
   }


   private static boolean lineContainsClose(String line) {
      outerloop:
      for(String[] values : TranslateTable.getCloseStuff()) {
         if (!line.substring(Integer.parseInt(values[0]) - 1, Integer.parseInt(values[0]) + Integer.parseInt(values[1]) - 1).equalsIgnoreCase(values[2]))
            return false;
      }
      return true;
   }

   static void processSERecords(ArrayList<String> answers, int maxRecords) {
      String master = answers.get(0);
      String second = answers.get(1);
      String output = answers.get(9);
      String mReject = answers.get(3);
      String sReject = answers.get(2);
      int mCardCount = Integer.parseInt(answers.get(10));
      int sCardCount = Integer.parseInt(answers.get(11));
      int oCardCount = Integer.parseInt(answers.get(12));
      String duplicates = answers.get(7);
      int length = Integer.parseInt(answers.get(6));
      int mCol = Integer.parseInt(answers.get(4));
      int sCol = Integer.parseInt(answers.get(5));
      String closed = answers.get(14);

      try {
         int offset = 0;
         int update = 1000;
         int oLineLength = 120;
         int iLineLength = 120;
         int recordCounter = 0;
         if (Record.getCommands().get(0)[0].equals("FA")) {
            iLineLength = Integer.parseInt(Record.getCommands().get(0)[1]);
            oLineLength = Integer.parseInt(Record.getCommands().get(0)[2]);
            Record.getCommands().remove(0);
            offset++;
         }
         for (int x = 0; x < Record.getCommands().size(); x++) {
            if (Record.getCommands().get(x)[0].equals("MC")) {
               update = Integer.parseInt(Record.getCommands().get(x)[1]);
               Record.getCommands().remove(x);
               offset++;
            }
         }
         if (!Verify.verifyInput(sCardCount, oCardCount, iLineLength, oLineLength, offset))
            return;
         BufferedReader masterReader = new BufferedReader(new FileReader(master));
         BufferedReader secondReader = new BufferedReader(new FileReader(second));
         PrintWriter outputWriter = new PrintWriter(new FileWriter(output));
         PrintWriter mRejectWriter = new PrintWriter(new FileWriter(mReject));
         PrintWriter sRejectWriter = new PrintWriter(new FileWriter(sReject));
         PrintWriter cWriter = new PrintWriter(new FileWriter(closed));



         boolean eofFile = false;
         boolean stop = false;
         ArrayList<String> sInput = new ArrayList<>();
         boolean dontSend = false;
         while (!eofFile) {
            ArrayList<String> input = new ArrayList<>();
            for (int x = 0; x < mCardCount; x++) {
               String line = masterReader.readLine();
               eofFile = (line == null);
               if (eofFile) {
                  break;
               }

               input.add(String.format("%-" + oLineLength + "." + oLineLength + "s", line));
            }
            if (!eofFile) {
               Record IORecord = new Record(input, oCardCount, oLineLength);
               for (int y = 1; y <= mCardCount; y++) {
                  IORecord.move(y, 1, oLineLength, y, 1);
               }

               if (duplicates.equalsIgnoreCase("ma")) {

                  boolean found = false;
                  if (sInput.isEmpty())
                     for (int x = 0; x < sCardCount; x++) {
                        String line = secondReader.readLine();
                        if (line != null) {
                           sInput.add(String.format("%-" + iLineLength + "." + iLineLength + "s",line));
                        } else {
                           stop = true;
                        }
                     }
                  if (!stop) {
                     while (!stop && (sInput.get((int)Math.ceil((sCol / iLineLength)))).substring(sCol % iLineLength - 1, sCol % iLineLength - 1 + length).compareTo((IORecord.getOutput().get((int)Math.ceil((mCol / oLineLength)))).substring(mCol % oLineLength - 1, mCol % oLineLength - 1 + length)) <= 0 && !sInput.isEmpty()) {
                        if ((sInput.get((int)Math.ceil((sCol / iLineLength)))).substring(sCol % iLineLength - 1, sCol % iLineLength - 1 + length).compareTo((IORecord.getOutput().get((int)Math.ceil((mCol / oLineLength)))).substring(mCol % oLineLength - 1, mCol % oLineLength - 1 + length)) == 0) {
                           IORecord = new Record(sInput, IORecord.getOutput());
                           IORecord.runCommands();
                           found = true;
                           dontSend = true;
                           break;
                        }
                        if (!dontSend)
                           for (String s : sInput) mRejectWriter.println(s);

                        dontSend = false;
                        for (int x = 0; x < sCardCount; x++) {
                           String line = secondReader.readLine();
                           if (line != null) {
                              sInput.remove(0);
                              sInput.add(String.format("%-" + iLineLength + "." + iLineLength + "s",line));
                           } else {
                              stop = true;
                              dontSend = true;

                              break;
                           }
                        }
                     }
                     if (!found)
                        for (int x = 0; x < IORecord.getOutput().size(); x++) {
                           sRejectWriter.println(IORecord.getOutput().get(x));
                        }
                  } else {
                     for (int x = 0; x < IORecord.getOutput().size(); x++)
                        sRejectWriter.println(IORecord.getOutput().get(x));
                  }
               } else if (duplicates.equalsIgnoreCase("se")) {

                  boolean found = false;
                  boolean keepGoing = false;
                  if (sInput.isEmpty())
                     for (int x = 0; x < sCardCount; x++) {
                        String line = secondReader.readLine();
                        if (line != null) {
                           sInput.add(String.format("%-" + iLineLength + "." + iLineLength + "s",line));
                        } else {
                           stop = true;
                        }
                     }
                  if (!stop) {
                     while (!stop && (sInput.get((int)Math.ceil((sCol / iLineLength)))).substring(sCol % iLineLength - 1, sCol % iLineLength - 1 + length).compareTo((IORecord.getOutput().get((int)Math.ceil((mCol / oLineLength)))).substring(mCol % oLineLength - 1, mCol % oLineLength - 1 + length)) <= 0 && !sInput.isEmpty()) {
                        if ((sInput.get((int)Math.ceil((sCol / iLineLength)))).substring(sCol % iLineLength - 1, sCol % iLineLength - 1 + length).compareTo((IORecord.getOutput().get((int)Math.ceil((mCol / oLineLength)))).substring(mCol % oLineLength - 1, mCol % oLineLength - 1 + length)) == 0) {
                           IORecord = new Record(sInput, IORecord.getOutput());
                           IORecord.runCommands();
                           found = true;
                           keepGoing = true;

                           if (!IORecord.getClosed()) {
                              for (int x = 0; x < IORecord.getOutput().size(); x++) {
                                 if (maxRecords < 0) {
                                    outputWriter.println(IORecord.getOutput().get(x));
                                 } else {
                                    System.out.println(IORecord.getOutput().get(x));
                                 }
                              }
                           }
                           if (IORecord.getClosed()) {
                              if (IORecord.getWriteOutput())
                              { for (int x = 0; x < IORecord.getOutput().size(); x++) {
                                 if (maxRecords < 0) {
                                    cWriter.println(IORecord.getOutput().get(x));
                                 } else {
                                    System.out.println(IORecord.getOutput().get(x));
                                 }
                              }  }
                              else { for (int x = 0; x < IORecord.getInput().size(); x++) {
                                 if (maxRecords < 0) {
                                    cWriter.println(IORecord.getInput().get(x));
                                 } else {
                                    System.out.println(IORecord.getInput().get(x));
                                 }
                              }  }

                           }



                           recordCounter++;
                           if (recordCounter % update == 0)
                              System.out.println("Successfully finished " + recordCounter + " records.");
                           if (recordCounter == maxRecords)
                              return;
                        } else {
                           for (String s : sInput) mRejectWriter.println(s);

                           for (int x = 0; x < sCardCount; x++) {
                              String line = secondReader.readLine();
                              sInput.remove(0);
                              if (line != null) {
                                 sInput.add(String.format("%-" + iLineLength + "." + iLineLength + "s",line));
                              } else {
                                 stop = true;
                                 break;
                              }
                           }
                        }
                        while (keepGoing && !stop) {
                           for (int x = 0; x < sCardCount; x++) {
                              sInput.remove(0);
                              String line = secondReader.readLine();
                              if (line != null) {
                                 sInput.add(String.format("%-" + iLineLength + "." + iLineLength + "s",line));
                              } else {
                                 stop = true;
                              }
                           }  if (!stop) {
                              if ((sInput.get(0)).substring(sCol - 1, sCol - 1 + length).compareTo((IORecord.getOutput().get(0)).substring(mCol - 1, mCol - 1 + length)) == 0) {
                                 IORecord = new Record(sInput, IORecord.getOutput());
                                 IORecord.runCommands();
                                 keepGoing = true;

                                 if (!IORecord.getClosed()) {
                                    for (int x = 0; x < IORecord.getOutput().size(); x++) {
                                       if (maxRecords < 0) {
                                          outputWriter.println(IORecord.getOutput().get(x));
                                       } else {
                                          System.out.println(IORecord.getOutput().get(x));
                                       }
                                    }
                                 }
                                 if (IORecord.getClosed()) {
                                    if (IORecord.getWriteOutput())
                                    { for (int x = 0; x < IORecord.getOutput().size(); x++) {
                                       if (maxRecords < 0) {
                                          cWriter.println(IORecord.getOutput().get(x));
                                       } else {
                                          System.out.println(IORecord.getOutput().get(x));
                                       }
                                    }  }
                                    else { for (int x = 0; x < IORecord.getInput().size(); x++) {
                                       if (maxRecords < 0) {
                                          cWriter.println(IORecord.getInput().get(x));
                                       } else {
                                          System.out.println(IORecord.getInput().get(x));
                                       }
                                    }  }

                                 }



                                 recordCounter++;
                                 if (recordCounter % update == 0)
                                    System.out.println("Successfully finished " + recordCounter + " records.");
                                 if (recordCounter == maxRecords)
                                    return;  continue;
                              }
                              keepGoing = false;
                           }
                        }
                     }  if (!found)
                        for (int x = 0; x < IORecord.getOutput().size(); x++) {
                           sRejectWriter.println(IORecord.getOutput().get(x));
                        }
                  } else {
                     for (int x = 0; x < IORecord.getOutput().size(); x++) {
                        sRejectWriter.println(IORecord.getOutput().get(x));
                     }
                  }
               } else {
                  boolean found = false;
                  if (sInput.isEmpty())
                     for (int x = 0; x < sCardCount; x++) {
                        String line = secondReader.readLine();
                        if (line != null) {
                           sInput.add(String.format("%-" + iLineLength + "." + iLineLength + "s",line));
                        } else {
                           stop = true;
                        }
                     }
                  if (!stop) {
                     while (!stop && (sInput.get((int)Math.ceil((sCol / iLineLength)))).substring(sCol % iLineLength - 1, sCol % iLineLength - 1 + length).compareTo((IORecord.getOutput().get((int)Math.ceil((mCol / oLineLength)))).substring(mCol % oLineLength - 1, mCol % oLineLength - 1 + length)) <= 0 && !sInput.isEmpty()) {
                        if ((sInput.get((int)Math.ceil((sCol / iLineLength)))).substring(sCol % iLineLength - 1, sCol % iLineLength - 1 + length).compareTo((IORecord.getOutput().get((int)Math.ceil((mCol / oLineLength)))).substring(mCol % oLineLength - 1, mCol % oLineLength - 1 + length)) == 0) {
                           IORecord = new Record(sInput, IORecord.getOutput());
                           IORecord.runCommands();
                           found = true;
                           if (sInput.size() > 0) {
                              sInput.subList(0, sInput.size()).clear();
                           }
                           break;
                        }
                        for (String s : sInput) mRejectWriter.println(s);

                        for (int x = 0; x < sCardCount; x++) {
                           String line = secondReader.readLine();
                           sInput.remove(0);
                           if (line != null) {
                              sInput.add(String.format("%-" + iLineLength + "." + iLineLength + "s",line));
                           } else {
                              stop = true;

                              break;
                           }
                        }
                     }
                     if (!found)
                        for (int x = 0; x < IORecord.getOutput().size(); x++) {
                           sRejectWriter.println(IORecord.getOutput().get(x));
                        }
                  } else {
                     for (int x = 0; x < IORecord.getOutput().size(); x++)
                        sRejectWriter.println(IORecord.getOutput().get(x));
                  }
               }
               if (IORecord.getRan() && !duplicates.equalsIgnoreCase("second")) {


                  if (!IORecord.getClosed()) {
                     for (int x = 0; x < IORecord.getOutput().size(); x++) {
                        if (maxRecords < 0) {
                           outputWriter.println(IORecord.getOutput().get(x));
                        } else {
                           System.out.println(IORecord.getOutput().get(x));
                        }
                     }
                  }
                  if (IORecord.getClosed()) {
                     if (IORecord.getWriteOutput())
                     { for (int x = 0; x < IORecord.getOutput().size(); x++) {
                        if (maxRecords < 0) {
                           cWriter.println(IORecord.getOutput().get(x));
                        } else {
                           System.out.println(IORecord.getOutput().get(x));
                        }
                     }  }
                     else { for (int x = 0; x < IORecord.getInput().size(); x++) {
                        if (maxRecords < 0) {
                           cWriter.println(IORecord.getInput().get(x));
                        } else {
                           System.out.println(IORecord.getInput().get(x));
                        }
                     }  }

                  }


                  recordCounter++;
                  if (recordCounter % update == 0)
                     System.out.println("Successfully finished " + recordCounter + " records.");
                  if (recordCounter == maxRecords) {
                     return;
                  }
               }
            }
         }

         if (duplicates.equalsIgnoreCase(" ") || duplicates.equalsIgnoreCase("second")) {
            String l;
            for (String s : sInput) mRejectWriter.println(s);
            do {
               l = secondReader.readLine();
               if (l == null)
                  continue;
               mRejectWriter.println(String.format("%-" + iLineLength + "." + iLineLength + "s", l ));
            } while (l != null);
         } else if (duplicates.equalsIgnoreCase("ma")) {
            String l; if (!dontSend) {
               for (String s : sInput) mRejectWriter.println(s);

            }
            do {
               l = secondReader.readLine();
               if (l == null)
                  continue;
               mRejectWriter.println(String.format("%-" + iLineLength + "." + iLineLength + "s", l ));
            } while (l != null);
         }

         outputWriter.close();
         cWriter.close();
         mRejectWriter.close();
         sRejectWriter.close();



      }
      catch (IOException e) {


         System.out.println("File not found");
      }
   }
}
