//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

class Setup {

   static void readTranslateTables(String file) {
      try {
         TranslateTable.setFileName(file);
         BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
         Record.getTTables().add(new TranslateTable());
         String ttLine;
         do {
            ttLine = reader.readLine();
            if (ttLine != null && !ttLine.substring(0, 1).equals("@") && ttLine.indexOf(44) != -1 && ttLine.charAt(0) != '+' && ttLine.split(",").length == 3) {
               (Record.getTTables().get(Record.getTTables().size() - 1)).addNumComp(ttLine);
               (Record.getTTables().get(Record.getTTables().size() - 1)).addTranslation(ttLine);
            } else if (ttLine != null && !ttLine.substring(0, 1).equals("@") && ttLine.indexOf(44) != -1 && ttLine.charAt(0) != '+') {
               (Record.getTTables().get(Record.getTTables().size() - 1)).addTranslation(ttLine);
            } else if (ttLine != null && ttLine.substring(0, 1).equals("+")) {
               Record.getTTables().add(new TranslateTable());
            } else if (ttLine != null && ttLine.indexOf(44) == -1) {
               (Record.getTTables().get(Record.getTTables().size() - 1)).addCloseKey(ttLine);
               TranslateTable.closeStuff.add(new String[] {"","", ttLine});
            }
         } while(ttLine != null);
         reader.close();
      } catch (IOException ignored) {
      }

   }

   public static ArrayList<String[]> readCommands(String formatFile) throws FileNotFoundException {

      ArrayList<String[]> commands = new ArrayList<>();

      Scanner reader = new Scanner(new File(formatFile));
      while (reader.hasNext()) {
         commands.add(reader.nextLine().split(","));

      }
      return commands;
   }

   static boolean[] getExtras(ArrayList<String[]> commands) {
      boolean[] reqs = new boolean[2];
      Iterator<String[]> var2 = commands.iterator();

      do {
         if (!var2.hasNext()) {
            return reqs;
         }

         String[] command = var2.next();
         String var4 = command[0];
         byte var5 = -1;
         switch(var4.hashCode()) {
            case 2142:
               if (var4.equals("CA")) {
                  var5 = 4;
               }
               break;
            case 2153:
               if (var4.equals("CL")) {
                  var5 = 0;
               }
               break;
            case 2161:
               if (var4.equals("CT")) {
                  var5 = 3;
               }
               break;
            case 2669:
               if (var4.equals("TA")) {
                  var5 = 2;
               }
               break;
            case 2670:
               if (var4.equals("TB")) {
                  var5 = 5;
               }
               break;
            case 2686:
               if (var4.equals("TR")) {
                  var5 = 1;
               }
         }

         switch(var5) {
            case 0:
               reqs[1] = true;
               break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
               reqs[0] = true;
         }
      } while(!reqs[0] || !reqs[1]);

      return reqs;
   }
}
