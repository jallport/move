//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

class Record {
   private final ArrayList<String> input;
   private final ArrayList<String> output;
   private int tTableCounter;
   private boolean closed;
   private boolean writeOutput;
   private static final ArrayList<TranslateTable> tTables = new ArrayList();
   private static ArrayList<String[]> commands;
   private boolean ran = false;
   public static ArrayList<String[]> closeValues = new ArrayList<>();

   public Record(ArrayList<String> input, int oCardCount, int oLineLength) {
      this.input = input;
      this.output = new ArrayList();
      this.tTableCounter = 0;
      this.closed = false;
      this.writeOutput = true;

      for(int x = 0; x < oCardCount; ++x) {
         String line = String.format("%-" + oLineLength + "." + oLineLength + "s", " ");
         this.output.add(line);
      }

   }

   public Record(ArrayList<String> input, ArrayList<String> output) {
      this.input = input;
      this.output = output;
      this.tTableCounter = 0;
      this.closed = false;
      this.writeOutput = true;
   }

   public static void setCommands(ArrayList<String[]> cmds) {
      commands = cmds;
   }

   public static ArrayList<String[]> getCommands() {
      return commands;
   }

   public boolean getRan() {
      return this.ran;
   }

   public static ArrayList<TranslateTable> getTTables() {
      return tTables;
   }

   public ArrayList<String> getOutput() {
      return this.output;
   }

   public ArrayList<String> getInput() {
      return this.input;
   }

   public void increaseTTableCounter() {
      ++this.tTableCounter;
   }

   public boolean getWriteOutput() {
      return this.writeOutput;
   }

   public void runCommands() {

      for (String[] strings : commands) {
         String command = strings[0];
         byte var5 = -1;
         switch (command.hashCode()) {
            case 2116:
               if (command.equals("BF")) {
                  var5 = 8;
               }
               break;
            case 2142:
               if (command.equals("CA")) {
                  var5 = 12;
               }
               break;
            case 2153:
               if (command.equals("CL")) {
                  var5 = 14;
               }
               break;
            case 2154:
               if (command.equals("CM")) {
                  var5 = 2;
               }
               break;
            case 2156:
               if (command.equals("CO")) {
                  var5 = 0;
               }
               break;
            case 2159:
               if (command.equals("CR")) {
                  var5 = 1;
               }
               break;
            case 2161:
               if (command.equals("CT")) {
                  var5 = 11;
               }
               break;
            case 2243:
               if (command.equals("FI")) {
                  var5 = 7;
               }
               break;
            case 2425:
               if (command.equals("LE")) {
                  var5 = 6;
               }
               break;
            case 2452:
               if (command.equals("MA")) {
                  var5 = 4;
               }
               break;
            case 2466:
               if (command.equals("MO")) {
                  var5 = 3;
               }
               break;
            case 2615:
               if (command.equals("RI")) {
                  var5 = 5;
               }
               break;
            case 2669:
               if (command.equals("TA")) {
                  var5 = 10;
               }
               break;
            case 2670:
               if (command.equals("TB")) {
                  var5 = 13;
               }
               break;
            case 2686:
               if (command.equals("TR")) {
                  var5 = 9;
               }
         }

         switch (var5) {
            case 0:
            default:
               break;
            case 1:
               this.crunch(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
               break;
            case 2:
               this.compress(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
               break;
            case 3:
               this.move(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
               break;
            case 4:
               this.moveAround(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
               break;
            case 5:
               this.rightJustify(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
               break;
            case 6:
               this.leftJustify(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
               break;
            case 7:
               if (strings[3].charAt(0) == '"' && strings[3].charAt(strings[3].length() - 1) == '"') {
                  strings[3] = strings[3].substring(1, strings[3].length() - 1);
               }

               this.fill(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), strings[3]);
               break;
            case 8:
               this.blankFill(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]));
               break;
            case 9:
               if (strings.length == 8) {
                  if (strings[7].equalsIgnoreCase("t")) {
                     strings[7] = "true";
                  } else {
                     strings[7] = "false";
                  }

                  this.translate(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), Boolean.parseBoolean(strings[7]));
               } else {
                  this.translate(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), false);
               }
               break;
            case 10:
               if (strings.length == 8) {
                  if (strings[7].equalsIgnoreCase("t")) {
                     strings[7] = "true";
                  } else {
                     strings[7] = "false";
                  }

                  this.translateAround(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), Boolean.parseBoolean(strings[7]));
               } else {
                  this.translateAround(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), false);
               }
               break;
            case 11:
               if (strings.length == 8) {
                  if (strings[7].equalsIgnoreCase("t")) {
                     strings[7] = "true";
                  } else {
                     strings[7] = "false";
                  }

                  this.conditionalTranslate(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), Boolean.parseBoolean(strings[7]));
               } else {
                  this.conditionalTranslate(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), false);
               }
               break;
            case 12:
               if (strings.length == 8) {
                  if (strings[7].equalsIgnoreCase("t")) {
                     strings[7] = "true";
                  } else {
                     strings[7] = "false";
                  }

                  this.conditionalTranslateAround(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), Boolean.parseBoolean(strings[7]));
               } else {
                  this.conditionalTranslateAround(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]), false);
               }
               break;
            case 13:
               this.translateBetween(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]), Integer.parseInt(strings[6]));
               break;
            case 14:
               if (strings[4].equalsIgnoreCase("T")) {
                  strings[4] = "true";
               }

               if (strings[4].equalsIgnoreCase("F")) {
                  strings[4] = "false";
               }

               this.closed(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), Boolean.parseBoolean(strings[4]), strings[5].charAt(0), strings[6].charAt(0));
         }
      }

      this.ran = true;
   }

   private void translate(int iRow, int iCol, int iLength, int oRow, int oCol, int oLength, boolean bit) {
      String line = this.input.get(iRow - 1);
      String segment = line.substring(iCol - 1, iCol - 1 + iLength);
      String[] segArr = segment.split("");
      boolean isMatch = true;
      boolean isDone = false;

      for(int x = 0; x < (getTTables().get(this.tTableCounter)).getKeys().size(); ++x) {
         String[] keyArr = ((getTTables().get(this.tTableCounter)).getKeys().get(x)).split("");
         if (keyArr[0].equals("*")) {
            isMatch = true;
         }

         if (!isDone) {
            isMatch = true;

            for(int y = 0; y < segArr.length; ++y) {
               if (!segArr[y].equals(keyArr[y]) && !keyArr[y].equals("#")) {
                  isMatch = false;
                  break;
               }
            }
         }

         if (isMatch) {
            StringBuilder newKey = new StringBuilder();

            for(int y = 0; y < keyArr.length; ++y) {
               if (keyArr[y].equals("#")) {
                  newKey.append(segArr[y]);
               } else {
                  newKey.append(keyArr[y]);
               }
            }

            String[] newKeyArr = newKey.toString().split("");
            String[] repArr = ((getTTables().get(this.tTableCounter)).getReplacements().get(x)).split("");
            String newReplacement = "";
            int poundCount = 0;
            String[] var20 = repArr;
            int var21 = repArr.length;

            for(int var22 = 0; var22 < var21; ++var22) {
               String s = var20[var22];
               if (s.equals("#")) {
                  newReplacement = newReplacement + newKeyArr[poundCount];
                  ++poundCount;
               } else if (s.equals("@")) {
                  ++poundCount;
               } else if (s.equals(" ")) {
                  if (bit) {
                     newReplacement = newReplacement + " ";
                     ++poundCount;
                  } else {
                     newReplacement = newReplacement + " ";
                  }
               } else {
                  newReplacement = newReplacement + s;
               }
            }

            this.translateFill(oRow, oCol, String.format("%-" + oLength + "." + oLength + "s", newReplacement));
            ++this.tTableCounter;
            return;
         }
      }

      ++this.tTableCounter;
   }

   private void translateAround(int iRow, int iCol, int iLength, int oRow, int oCol, int oLength, boolean bit) {
      String line = this.output.get(iRow - 1);
      String segment = line.substring(iCol - 1, iCol - 1 + iLength);
      String[] segArr = segment.split("");
      boolean isMatch = true;
      boolean isDone = false;

      for(int x = 0; x < (getTTables().get(this.tTableCounter)).getKeys().size(); ++x) {
         String[] keyArr = ((getTTables().get(this.tTableCounter)).getKeys().get(x)).split("");
         if (keyArr[0].equals("*")) {
            isMatch = true;
         }

         if (!isDone) {
            isMatch = true;

            for(int y = 0; y < segArr.length; ++y) {
               if (!segArr[y].equals(keyArr[y]) && !keyArr[y].equals("#")) {
                  isMatch = false;
               }
            }
         }

         if (isMatch) {
            String newKey = "";

            for(int y = 0; y < keyArr.length; ++y) {
               if (keyArr[y].equals("#")) {
                  newKey = newKey + segArr[y];
               } else {
                  newKey = newKey + keyArr[y];
               }
            }

            String[] newKeyArr = newKey.split("");
            String[] repArr = ((getTTables().get(this.tTableCounter)).getReplacements().get(x)).split("");
            String newReplacement = "";
            int poundCount = 0;
            String[] var20 = repArr;
            int var21 = repArr.length;

            for(int var22 = 0; var22 < var21; ++var22) {
               String s = var20[var22];
               if (s.equals("#")) {
                  newReplacement = newReplacement + newKeyArr[poundCount];
                  ++poundCount;
               } else if (s.equals("@")) {
                  ++poundCount;
               } else if (s.equals(" ")) {
                  if (bit) {
                     newReplacement = newReplacement + " ";
                     ++poundCount;
                  } else {
                     newReplacement = newReplacement + " ";
                  }
               } else {
                  newReplacement = newReplacement + s;
               }
            }

            this.translateFill(oRow, oCol, String.format("%-" + oLength + "." + oLength + "s", newReplacement));
            ++this.tTableCounter;
            return;
         }
      }

      ++this.tTableCounter;
   }

   private void conditionalTranslate(int iRow, int iCol, int iLength, int oRow, int oCol, int oLength, boolean bit) {
      String line = this.input.get(iRow - 1);
      String segment = line.substring(iCol - 1, iCol - 1 + iLength);
      String[] segArr = segment.split("");
      boolean isMatch = true;
      boolean isDone = false;

      for(int x = 0; x < (getTTables().get(this.tTableCounter)).getKeys().size(); ++x) {
         String[] keyArr = ((getTTables().get(this.tTableCounter)).getKeys().get(x)).split("");
         if (keyArr[0].equals("*")) {
            isMatch = true;
         }

         if (!isDone) {
            isMatch = true;

            for(int y = 0; y < segArr.length; ++y) {
               if (!segArr[y].equals(keyArr[y]) && !keyArr[y].equals("#")) {
                  isMatch = false;
               }
            }
         }

         if (isMatch) {
            String newKey = "";

            for(int y = 0; y < keyArr.length; ++y) {
               if (keyArr[y].equals("#")) {
                  newKey = newKey + segArr[y];
               } else {
                  newKey = newKey + keyArr[y];
               }
            }

            String[] newKeyArr = newKey.split("");
            String[] repArr = ((getTTables().get(this.tTableCounter)).getReplacements().get(x)).split("");
            String newReplacement = "";
            int poundCount = 0;
            String[] var20 = repArr;
            int var21 = repArr.length;

            for(int var22 = 0; var22 < var21; ++var22) {
               String s = var20[var22];
               if (s.equals("#")) {
                  newReplacement = newReplacement + newKeyArr[poundCount];
                  ++poundCount;
               } else if (s.equals("@")) {
                  ++poundCount;
               } else if (s.equals(" ")) {
                  if (bit) {
                     newReplacement = newReplacement + " ";
                     ++poundCount;
                  } else {
                     newReplacement = newReplacement + " ";
                  }
               } else {
                  newReplacement = newReplacement + s;
               }
            }

            this.conditionalTranslateFill(oRow, oCol, String.format("%-" + oLength + "." + oLength + "s", newReplacement));
            ++this.tTableCounter;
            return;
         }
      }

      ++this.tTableCounter;
   }

   private void conditionalTranslateAround(int iRow, int iCol, int iLength, int oRow, int oCol, int oLength, boolean bit) {
      String line = this.output.get(iRow - 1);
      String segment = line.substring(iCol - 1, iCol - 1 + iLength);
      String[] segArr = segment.split("");
      boolean isMatch = true;
      boolean isDone = false;

      for(int x = 0; x < (getTTables().get(this.tTableCounter)).getKeys().size(); ++x) {
         String[] keyArr = ((getTTables().get(this.tTableCounter)).getKeys().get(x)).split("");
         if (keyArr[0].equals("*")) {
            isMatch = true;
         }

         if (!isDone) {
            isMatch = true;

            for(int y = 0; y < segArr.length; ++y) {
               if (!segArr[y].equals(keyArr[y]) && !keyArr[y].equals("#")) {
                  isMatch = false;
               }
            }
         }

         if (isMatch) {
            String newKey = "";

            for(int y = 0; y < keyArr.length; ++y) {
               if (keyArr[y].equals("#")) {
                  newKey = newKey + segArr[y];
               } else {
                  newKey = newKey + keyArr[y];
               }
            }

            String[] newKeyArr = newKey.split("");
            String[] repArr = ((getTTables().get(this.tTableCounter)).getReplacements().get(x)).split("");
            String newReplacement = "";
            int poundCount = 0;
            String[] var20 = repArr;
            int var21 = repArr.length;

            for(int var22 = 0; var22 < var21; ++var22) {
               String s = var20[var22];
               if (s.equals("#")) {
                  newReplacement = newReplacement + newKeyArr[poundCount];
                  ++poundCount;
               } else if (s.equals("@")) {
                  ++poundCount;
               } else if (s.equals(" ")) {
                  if (bit) {
                     newReplacement = newReplacement + " ";
                     ++poundCount;
                  } else {
                     newReplacement = newReplacement + " ";
                  }
               } else {
                  newReplacement = newReplacement + s;
               }
            }

            this.conditionalTranslateFill(oRow, oCol, String.format("%-" + oLength + "." + oLength + "s", newReplacement));
            ++this.tTableCounter;
            return;
         }
      }

      ++this.tTableCounter;
   }

   private void translateBetween(int iCard, int iCol, int iLength, int oCard, int oCol, int oLength) {
      String line = this.input.get(iCard - 1);
      int middle = Integer.parseInt(line.substring(iCol - 1, iCol - 1 + iLength));

      for(int x = 0; x < (getTTables().get(this.tTableCounter)).getUppers().size(); ++x) {
         if (((getTTables().get(this.tTableCounter)).getLowers().get(x)).charAt(0) == '*') {
            this.conditionalTranslateFill(oCard, oCol, String.format("%-" + oLength + "." + oLength + "s", (getTTables().get(this.tTableCounter)).getRNums().get(x)));
            ++this.tTableCounter;
            return;
         }

         if (middle >= Integer.parseInt((getTTables().get(this.tTableCounter)).getLowers().get(x)) && middle <= Integer.parseInt((getTTables().get(this.tTableCounter)).getUppers().get(x))) {
            this.conditionalTranslateFill(oCard, oCol, String.format("%-" + oLength + "." + oLength + "s", (getTTables().get(this.tTableCounter)).getRNums().get(x)));
            ++this.tTableCounter;
            return;
         }
      }

   }

   private void closed(int row, int col, int length, boolean ifMatch, char search, char write) {
      String line;
      if (search == 'I') {
         line = this.input.get(row - 1);
      } else {
         line = this.output.get(row - 1);
      }

      String segment = line.substring(col - 1, col - 1 + length);
      String[] segArr = segment.split("");

      for(int x = 0; x < (tTables.get(this.tTableCounter)).getCloseKeys().size(); ++x) {
         String[] closeKeyArr = ((tTables.get(this.tTableCounter)).getCloseKeys().get(x)).split("");
         boolean isMatch = true;

         for(int y = 0; y < segArr.length; ++y) {
            if (!segArr[y].equals(closeKeyArr[y]) && !closeKeyArr[y].equals("#")) {
               isMatch = false;
               break;
            }
         }

         if (isMatch && ifMatch) {
            this.closed = true;
            this.writeOutput = write != 'I';
         } else {
            if (isMatch) {
               return;
            }

            if (x == (tTables.get(this.tTableCounter)).getCloseKeys().size() - 1 && !ifMatch) {
               this.closed = true;
               this.writeOutput = write != 'I';
            }
         }
      }

   }

   boolean getClosed() {
      return this.closed;
   }

   private void conditionalTranslateFill(int card, int col, String ctx) {
      String line = this.output.get(card - 1);
      String first = line.substring(0, col - 1);
      String after = line.substring(col - 1 + ctx.length());
      this.output.set(card - 1, first + ctx + after);
   }

   private void translateFill(int card, int col, String ctx) {
      String line = this.output.get(card - 1);
      String first = line.substring(0, col - 1);
      String after = line.substring(col - 1 + ctx.length());
      String middle = line.substring(col - 1, col - 1 + ctx.length());
      String[] midArr = middle.split("");
      String[] ctxArr = ctx.split("");
      String newMiddle = "";

      for(int x = 0; x < ctxArr.length; ++x) {
         if (ctxArr[x].equals(" ")) {
            newMiddle = newMiddle + midArr[x];
         } else {
            newMiddle = newMiddle + ctxArr[x];
         }
      }

      this.output.set(card - 1, first + newMiddle + after);
   }

   private void fill(int card, int col, String ctx) {
      ctx = ctx.trim();
      String line = this.output.get(card - 1);
      String first = line.substring(0, col - 1);
      String after = line.substring(col - 1 + ctx.length());
      this.output.set(card - 1, first + ctx + after);
   }

   private void blankFill(int card, int col, int length) {
      String line = this.output.get(card - 1);
      String first = line.substring(0, col - 1);
      String after = line.substring(col - 1 + length);
      String spaces = String.format("%-" + length + "." + length + "s", " ");
      String fix = first + spaces + after;
      this.output.set(card - 1, fix);
   }

   public void moveBlank(int iRow, int iCol, int length, int oRow, int oCol) {
      String hold = (this.input.get(iRow - 1)).substring(iCol - 1, iCol - 1 + length);
      String line = this.output.get(oRow - 1);
      String before = line.substring(0, oCol - 1);
      String after = line.substring(oCol - 1 + length);
      this.output.set(oRow - 1, before + hold + after);
   }

   public void move(int iRow, int iCol, int length, int oRow, int oCol) {
      String hold = (this.input.get(iRow - 1)).substring(iCol - 1, iCol - 1 + length);
      String line = this.output.get(oRow - 1);
      String before = line.substring(0, oCol - 1);
      String middle = line.substring(oCol - 1, oCol - 1 + length);
      String after = line.substring(oCol - 1 + length);
      String[] middleArr = middle.split("");
      String[] holdArr = hold.split("");
      String fixMiddle = "";

      for(int x = 0; x < middleArr.length; ++x) {
         if (holdArr[x].equals(" ")) {
            holdArr[x] = middleArr[x];
         }

         fixMiddle = fixMiddle + holdArr[x];
      }

      this.output.set(oRow - 1, before + fixMiddle + after);
   }

   private void moveAround(int sRow, int sCol, int length, int eRow, int eCol) {
      String hold = (this.output.get(sRow - 1)).substring(sCol - 1, sCol - 1 + length);
      String line = this.output.get(eRow - 1);
      String before = line.substring(0, eCol - 1);
      String middle = line.substring(eCol - 1, eCol - 1 + length);
      String after = line.substring(eCol - 1 + length);
      String[] middleArr = middle.split("");
      String[] holdArr = hold.split("");
      String fixMiddle = "";

      for(int x = 0; x < middleArr.length; ++x) {
         if (holdArr[x].equals(" ")) {
            holdArr[x] = middleArr[x];
         }

         fixMiddle = fixMiddle + holdArr[x];
      }

      this.output.set(eRow - 1, before + fixMiddle + after);
   }

   private void leftJustify(int iRow, int iCol, int length, int oRow, int oCol) {
      this.move(iRow, iCol, length, oRow, oCol);
      String line = this.output.get(oRow - 1);
      String before = line.substring(0, oCol - 1);
      String middle = line.substring(oCol - 1, oCol - 1 + length);

      String after;
      for(after = line.substring(oCol - 1 + length); middle.charAt(0) == ' '; middle = middle.substring(1) + " ") {
      }

      this.output.set(oRow - 1, before + middle + after);
   }

   private void rightJustify(int iRow, int iCol, int length, int oRow, int oCol) {
      this.move(iRow, iCol, length, oRow, oCol);
      String line = this.output.get(oRow - 1);
      String before = line.substring(0, oCol - 1);
      String middle = line.substring(oCol - 1, oCol - 1 + length);

      String after;
      for(after = line.substring(oCol - 1 + length); middle.charAt(middle.length() - 1) == ' '; middle = " " + middle.substring(0, middle.length() - 1)) {
      }

      this.output.set(oRow - 1, before + middle + after);
   }

   private void crunch(int iRow, int iCol, int length, int oRow, int oCol) {
      String line = this.input.get(iRow - 1);
      String segment = line.substring(iCol - 1, iCol - 1 + length);
      segment = segment.replaceAll(" ", "");
      this.fill(oRow, oCol, segment);
   }

   private void compress(int iRow, int iCol, int length, int oRow, int oCol) {
      String line = this.input.get(iRow - 1);
      String segment = line.substring(iCol - 1, iCol - 1 + length);

      for(segment = segment.trim(); segment.contains("  "); segment = segment.replaceAll("  ", " ")) {
      }

      this.fill(oRow, oCol, segment);
   }
}
