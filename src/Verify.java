//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.util.ArrayList;

class Verify {

   static boolean verifyFormatFile() {
      int x = -1; //counter start value within loop at 0
      for(  String[] commands : Record.getCommands()) {
         x ++;
         String var3 = commands[0];
         byte var4 = -1;
         switch (var3.hashCode()) {
            case 2116:
               if (var3.equals("BF")) {
                  var4 = 10;
               }
               break;
            case 2142:
               if (var3.equals("CA")) {
                  var4 = 14;
               }
               break;
            case 2153:
               if (var3.equals("CL")) {
                  var4 = 16;
               }
               break;
            case 2154:
               if (var3.equals("CM")) {
                  var4 = 4;
               }
               break;
            case 2156:
               if (var3.equals("CO")) {
                  var4 = 0;
               }
               break;
            case 2159:
               if (var3.equals("CR")) {
                  var4 = 3;
               }
               break;
            case 2161:
               if (var3.equals("CT")) {
                  var4 = 13;
               }
               break;
            case 2235:
               if (var3.equals("FA")) {
                  var4 = 2;
               }
               break;
            case 2243:
               if (var3.equals("FI")) {
                  var4 = 9;
               }
               break;
            case 2425:
               if (var3.equals("LE")) {
                  var4 = 8;
               }
               break;
            case 2452:
               if (var3.equals("MA")) {
                  var4 = 6;
               }
               break;
            case 2454:
               if (var3.equals("MC")) {
                  var4 = 1;
               }
               break;
            case 2466:
               if (var3.equals("MO")) {
                  var4 = 5;
               }
               break;
            case 2615:
               if (var3.equals("RI")) {
                  var4 = 7;
               }
               break;
            case 2669:
               if (var3.equals("TA")) {
                  var4 = 12;
               }
               break;
            case 2670:
               if (var3.equals("TB")) {
                  var4 = 15;
               }
               break;
            case 2686:
               if (var3.equals("TR")) {
                  var4 = 11;
               }
         }

         switch (var4) {
            case 0:
               break;
            case 1:
               if (commands.length == 2 && Functions.isNumeric(commands[1]) && Integer.parseInt(commands[1]) >= 1) {
                  break;
               }

               System.out.println("Incorrect Format: Modify Counter -Command " + (x + 1));
               return false;
            case 2:
               if (commands.length == 3 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Integer.parseInt(commands[1]) >= 0 && Integer.parseInt(commands[2]) >= 0) {
                  break;
               }

               System.out.println("Incorrect Format: Fast Move -Command " + (x + 1));
               return false;
            case 3:
               if (commands.length == 6 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5])) {
                  break;
               }

               System.out.println("Incorrect Format: Crunch -Command " + (x + 1));
               return false;
            case 4:
               if (commands.length == 6 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5])) {
                  break;
               }

               System.out.println("Incorrect Format: Compress -Command " + (x + 1));
               return false;
            case 5:
               if (commands.length == 6 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5])) {
                  break;
               }

               System.out.println("Incorrect Format: Move -Command " + (x + 1));
               return false;
            case 6:
               if (commands.length == 6 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5])) {
                  break;
               }

               System.out.println("Incorrect Format: Move Around -Command " + (x + 1));
               return false;
            case 7:
               if (commands.length == 6 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5])) {
                  break;
               }

               System.out.println("Incorrect Format: Right Justify -Command " + (x + 1));
               return false;
            case 8:
               if (commands.length == 6 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5])) {
                  break;
               }

               System.out.println("Incorrect Format: Left Justify -Command " + (x + 1));
               return false;
            case 9:
               if (commands.length == 4 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2])) {
                  break;
               }

               System.out.println("Incorrect Format: Fill -Command" + (x + 1));
               return false;
            case 10:
               if (commands.length != 4 || !Functions.isNumeric(commands[1]) || !Functions.isNumeric(commands[2]) || !Functions.isNumeric(commands[3])) {
                  System.out.println("Incorrect Format: Blank Fill -Command" + (x + 1));
                  return false;
               }
               break;
            case 11:
               if (commands.length == 7 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) || commands.length == 8 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) && (commands[7].equalsIgnoreCase("T") || commands[7].equalsIgnoreCase("F"))) {
                  break;
               }

               System.out.println("Incorrect Format: Translate -Command" + (x + 1));
               return false;
            case 12:
               if (commands.length == 7 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) || commands.length == 8 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) && (commands[7].equalsIgnoreCase("T") || commands[7].equalsIgnoreCase("F"))) {
                  break;
               }

               System.out.println("Incorrect Format: Translate Around -Command" + (x + 1));
               return false;
            case 13:
               if (commands.length == 7 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) || commands.length == 8 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) && (commands[7].equalsIgnoreCase("T") || commands[7].equalsIgnoreCase("F"))) {
                  break;
               }

               System.out.println("Incorrect Format: Conditional Translate -Command" + (x + 1));
               return false;
            case 14:
               if (commands.length == 7 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) || commands.length == 8 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6]) && (commands[7].equalsIgnoreCase("T") || commands[7].equalsIgnoreCase("F"))) {
                  break;
               }

               System.out.println("Incorrect Format: Conditional Translate Around -Command" + (x + 1));
               return false;
            case 15:
               if (commands.length == 7 && Functions.isNumeric(commands[1]) && Functions.isNumeric(commands[2]) && Functions.isNumeric(commands[3]) && Functions.isNumeric(commands[4]) && Functions.isNumeric(commands[5]) && Functions.isNumeric(commands[6])) {
                  break;
               }

               System.out.println("Incorrect Format: Translate Between -Command" + (x + 1));
               return false;
            case 16:
               if (commands.length != 7 || !Functions.isNumeric(commands[1]) || !Functions.isNumeric(commands[2]) || !Functions.isNumeric(commands[3]) || !commands[4].equalsIgnoreCase("T") && !commands[4].equalsIgnoreCase("F") || !commands[5].equalsIgnoreCase("I") && !commands[5].equalsIgnoreCase("O") || !commands[6].equalsIgnoreCase("I") && !commands[6].equalsIgnoreCase("O")) {
                  System.out.println("Incorrect Format: Closed -Command" + (x + 1));
                  return false;
               }
               break;
            default:
               System.out.println("Unknown Command -Command " + (x + 1));
               return false;
         }
      }
      System.out.println("Format File Verified");
      return true;
   }

   static boolean verifyTranslateTables() {
      ArrayList<String[]> commands = Record.getCommands();
      ArrayList<TranslateTable> tables = Record.getTTables();
      int numTranslates = 0;
      for (String[] command : commands) {
         if (command[0].equalsIgnoreCase("TR") || command[0].equalsIgnoreCase("TA") || command[0].equalsIgnoreCase("CT")
                 || command[0].equalsIgnoreCase("CA") || command[0].equalsIgnoreCase("TB") || command[0].equalsIgnoreCase("CL")) {
            if (command[0].equalsIgnoreCase("cl")) {
               TranslateTable.getCloseStuff().get(numTranslates)[0] = command[2];
               TranslateTable.getCloseStuff().get(numTranslates)[1] = command[3];
            }
            numTranslates++;
         }
      }

      if (numTranslates != tables.size()) {
         System.out.println("Incorrect number of translate tables - # of translates -" + numTranslates + " # of tables -" + tables.size());
         return false;
      }
      for (int x = 0; x < tables.size(); x++) {
         if ((tables.get(x)).getKeys().size() != (tables.get(x)).getReplacements().size()) {
            System.out.println("Incorrect translate format -Table " + (x + 1));
            return false;
         }
         if ((tables.get(x)).getLowers().size() != (tables.get(x)).getUppers().size() || (tables.get(x)).getLowers().size() != (tables.get(x)).getRNums().size() || (tables
                 .get(x)).getUppers().size() != (tables.get(x)).getRNums().size()) {
            System.out.println("Incorrect translate format -Table " + (x + 1));
            return false;
         }
      }
      System.out.println("Translate Tables Verified");
      return true;
   }


   static boolean verifyInput(int iCardCount, int oCardCount, int iLineLength, int oLineLength, int offset) {
      ArrayList<String[]> commands = Record.getCommands();

      for(int x = 0; x < commands.size(); ++x) {
         String[] array = commands.get(x);
         String var8 = array[0];
         byte var9 = -1;
         switch(var8.hashCode()) {
            case 2116:
               if (var8.equals("BF")) {
                  var9 = 8;
               }
               break;
            case 2142:
               if (var8.equals("CA")) {
                  var9 = 12;
               }
               break;
            case 2153:
               if (var8.equals("CL")) {
                  var9 = 14;
               }
               break;
            case 2154:
               if (var8.equals("CM")) {
                  var9 = 2;
               }
               break;
            case 2156:
               if (var8.equals("CO")) {
                  var9 = 0;
               }
               break;
            case 2159:
               if (var8.equals("CR")) {
                  var9 = 1;
               }
               break;
            case 2161:
               if (var8.equals("CT")) {
                  var9 = 11;
               }
               break;
            case 2243:
               if (var8.equals("FI")) {
                  var9 = 7;
               }
               break;
            case 2425:
               if (var8.equals("LE")) {
                  var9 = 6;
               }
               break;
            case 2452:
               if (var8.equals("MA")) {
                  var9 = 4;
               }
               break;
            case 2466:
               if (var8.equals("MO")) {
                  var9 = 3;
               }
               break;
            case 2615:
               if (var8.equals("RI")) {
                  var9 = 5;
               }
               break;
            case 2669:
               if (var8.equals("TA")) {
                  var9 = 10;
               }
               break;
            case 2670:
               if (var8.equals("TB")) {
                  var9 = 13;
               }
               break;
            case 2686:
               if (var8.equals("TR")) {
                  var9 = 9;
               }
         }

         switch(var9) {
            case 0:
            default:
               break;
            case 1:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Crunch:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Crunch:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Crunch:" + (x + offset + 1));
                  return false;
               }
               break;
            case 2:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Compress:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Compress:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Compress:" + (x + offset + 1));
                  return false;
               }
               break;
            case 3:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Move:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Move:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Move:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[3]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Move:" + (x + offset + 1));
                  return false;
               }
               break;
            case 4:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > oCardCount) {
                  System.out.println("Invalid output Card Number -Move Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Move Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Move Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[3]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Move Around:" + (x + offset + 1));
                  return false;
               }
               break;
            case 5:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Right Justify:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Right Justify:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Right Justify:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[3]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Right Justify:" + (x + offset + 1));
                  return false;
               }
               break;
            case 6:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Left Justify:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Left Justify:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Left Justify:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[3]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Left Justify:" + (x + offset + 1));
                  return false;
               }
               break;
            case 7:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > oCardCount) {
                  System.out.println("Invalid output card number -Fill:" + (x + offset + 1));
               }

               if (Integer.parseInt(array[2]) - 1 + array[3].length() > oLineLength) {
                  System.out.println("Invalid output col and length -Fill:" + (x + offset + 1));
               }
               break;
            case 8:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > oCardCount) {
                  System.out.println("Invalid output card number -Blank Fill:" + (x + offset + 1));
               }

               if (Integer.parseInt(array[2]) - 1 + Integer.parseInt(array[3]) > oLineLength) {
                  System.out.println("Invalid output col and length -Blank Fill:" + (x + offset + 1));
               }
               break;
            case 9:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Translate:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Translate:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Translate:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[6]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Translate:" + (x + offset + 1));
                  return false;
               }
               break;
            case 10:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > oCardCount) {
                  System.out.println("Invalid Input Card Number -Translate Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Translate Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Translate Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[6]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Translate Around:" + (x + offset + 1));
                  return false;
               }
               break;
            case 11:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Conditional Translate:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Conditional Translate:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Conditional Translate:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[6]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length - Conditional Translate:" + (x + offset + 1));
                  return false;
               }
               break;
            case 12:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > oCardCount) {
                  System.out.println("Invalid Input Card Number -Conditional Translate Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Conditional Translate Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Conditional Translate Around:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[6]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Conditional Translate Around:" + (x + offset + 1));
                  return false;
               }
               break;
            case 13:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Translate Between:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Translate Between:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[4]) < 1 || Integer.parseInt(array[4]) > oCardCount) {
                  System.out.println("Invalid output card number -Translate Between:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[5]) + Integer.parseInt(array[6]) - 1 > oLineLength) {
                  System.out.println("Invalid output col and length -Translate Between:" + (x + offset + 1));
                  return false;
               }
               break;
            case 14:
               if (Integer.parseInt(array[1]) < 1 || Integer.parseInt(array[1]) > iCardCount) {
                  System.out.println("Invalid Input Card Number -Closed:" + (x + offset + 1));
                  return false;
               }

               if (Integer.parseInt(array[3]) + Integer.parseInt(array[2]) - 1 > iLineLength) {
                  System.out.println("Invalid input col and length -Closed:" + (x + offset + 1));
                  return false;
               }
         }
      }

      System.out.println("Input Verified");
      return true;
   }
}
