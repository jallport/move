import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

class Ask {
   public static int forReal() {
      String input;
      Scanner scanner = new Scanner(System.in);

      do {
         System.out.print("For real? ");
         input = scanner.next();
      } while (!input.equalsIgnoreCase("y") && !input.equalsIgnoreCase("n"));
      if (input.equalsIgnoreCase("y")) {
         return -1;
      }
      do {
         System.out.print("How many records? ");
         input = scanner.nextLine();
      } while (!Functions.isNumeric(input) || Integer.parseInt(input) <= 0);
      return Integer.parseInt(input);
   }



   public static ArrayList<String> getAnswers(String input) throws FileNotFoundException {
      File tmpDir;
      ArrayList<String> answers = new ArrayList<String>();
      Scanner scanner = new Scanner(System.in);

      if (input == null) {
         System.out.print("Existing Answer File? ");
         input = scanner.nextLine();
         tmpDir = new File(input);
      }
      else {

         tmpDir = new File(input);
      }

      if (tmpDir.exists()) {
         try {
            Scanner reader = new Scanner(new FileReader(input));
            reader.nextLine();
            reader.nextLine();
            while (reader.hasNext()) {
               answers.add(reader.nextLine().trim());
            }
         } catch (IOException ignored) {}
      } else {


         do {


            System.out.print("Master Input File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
         } while (!tmpDir.exists());
         answers.add(input);

         do {
            System.out.print("Format File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
         } while (!tmpDir.exists());
         answers.add(input);

         boolean[] ex = Setup.getExtras(Setup.readCommands(answers.get(1)));
         boolean exists = false;
         boolean canMake = false;
         do {
            System.out.print("Output File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
            exists = tmpDir.exists();
            if (exists)
               continue;  try {
               File file = new File(input);
               canMake = file.createNewFile();
            } catch (IOException iOException) {}

         }
         while (!exists && !canMake);
         answers.add(input);

         do {
            System.out.print("# of Input Cards");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         do {
            System.out.print("# of Output Cards");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         if (ex[0]) {
            do {
               System.out.print("Translate Table File? ");
               input = scanner.nextLine();
               tmpDir = new File(input);
            } while (!tmpDir.exists());
            answers.add(input);
         } else {
            answers.add(" ");
         }
         if (ex[1]) {
            do {
               System.out.print("Close Output File? ");
               input = scanner.nextLine();
               tmpDir = new File(input);
            } while (!tmpDir.exists());
            answers.add(input);
         } else {
            answers.add(" ");
         }

         exists = false;
         canMake = false;
         boolean exit = false;
         do {
            System.out.print("Create Answer File? ");
            input = scanner.nextLine();
            if (input.equals(" ")) {
               exit = true;
            } else {
               tmpDir = new File(input);
               exists = tmpDir.exists();
               if (!exists) {
                  try {
                     File file = new File(input);
                     canMake = file.createNewFile();
                  } catch (IOException iOException) {}
               }
            }

         } while (!exists && !canMake && !exit);
         if (!exit) {
            try {
               PrintWriter writer = new PrintWriter(new FileWriter(input));
               writer.println("BLAAAAHAHAHHAHA Date");
               writer.println("NUMBERS THAT I DONT CARE ABOUT");
               writer.println(answers.get(0));
               writer.println(answers.get(1));
               writer.println(answers.get(2));

               String iCard = answers.get(3);
               String oCard = answers.get(4);
               while (iCard.length() < 8)
                  iCard = " " + iCard;
               while (oCard.length() < 8)
                  oCard = " " + oCard;
               writer.println(iCard);
               writer.println(oCard);
               if (!(answers.get(5)).equals(" "))
                  writer.println(answers.get(5));
               if (!(answers.get(6)).equals(" "))
                  writer.println(answers.get(6));
               writer.close();
            } catch (IOException iOException) {}
         }
      }


      return answers;
   }
   static ArrayList<String> getSEAnswers(String input) throws FileNotFoundException {
      File tmpDir;
      ArrayList<String> answers = new ArrayList<String>();
      Scanner scanner = new Scanner(System.in);


      if (input == null) {
         System.out.print("Existing Answer File? ");
         input = scanner.nextLine();
         tmpDir = new File(input);
      } else {

         tmpDir = new File(input);
      }

      if (tmpDir.exists()) {
         try {
            BufferedReader reader = new BufferedReader(new FileReader(input));
            reader.readLine();
            reader.readLine();
            answers.add(reader.readLine());
            answers.add(reader.readLine());
            answers.add(reader.readLine());
            answers.add(reader.readLine());
            String s = reader.readLine();
            answers.add(s.substring(0, 8).trim());
            answers.add(s.substring(8, 16).trim());
            answers.add(s.substring(16).trim());
            answers.add(reader.readLine());




            answers.add(reader.readLine());
            answers.add(reader.readLine());
            answers.add(reader.readLine().trim());
            answers.add(reader.readLine().trim());
            answers.add(reader.readLine().trim());
            boolean[] ex = Setup.getExtras(Setup.readCommands(answers.get(8)));
            if (ex[0]) {
               answers.add(reader.readLine());
            } else {
               answers.add(" ");
            }  if (ex[1]) {
               answers.add(reader.readLine());
            } else {
               answers.add(" ");
            }

         } catch (IOException iOException) {}
      } else {


         do {
            System.out.print("Master Input File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
         } while (!tmpDir.exists());
         answers.add(input);

         do {
            System.out.print("Second Input File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
         } while (!tmpDir.exists());
         answers.add(input);

         boolean exists = false;
         boolean canMake = false;
         do {
            System.out.print("Second Reject File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
            exists = tmpDir.exists();
            if (exists)
               continue;  try {
               File file = new File(input);
               canMake = file.createNewFile();
            } catch (IOException iOException) {}

         }
         while (!exists && !canMake);
         answers.add(input);

         exists = false;
         canMake = false;
         do {
            System.out.print("Master Reject File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
            exists = tmpDir.exists();
            if (exists)
               continue;  try {
               File file = new File(input);
               canMake = file.createNewFile();
            } catch (IOException iOException) {}

         }
         while (!exists && !canMake);
         answers.add(input);

         do {
            System.out.print("Master Starting Column? ");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         do {
            System.out.print("Second Starting Column? ");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         do {
            System.out.print("Length? ");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         do {
            System.out.print("Does master or second input file contain duplicates? ");
            input = scanner.nextLine();
         } while (!input.equalsIgnoreCase("y") && !input.equalsIgnoreCase("n"));
         if (input.equalsIgnoreCase("y")) {
            do {
               System.out.print("Which one (MA or SE");
               input = scanner.nextLine();
            } while (!input.equalsIgnoreCase("MA") && !input.equalsIgnoreCase("SE"));
            answers.add(input);
         } else {
            answers.add("  ");
         }
         do {
            System.out.print("Format File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
         } while (!tmpDir.exists());
         answers.add(input);
         boolean[] ex = Setup.getExtras(Setup.readCommands(input));

         canMake = false;
         do {
            System.out.print("Output File? ");
            input = scanner.nextLine();
            tmpDir = new File(input);
            exists = tmpDir.exists();
            if (exists)
               continue;  try {
               File file = new File(input);
               canMake = file.createNewFile();
            } catch (IOException iOException) {}

         }
         while (!exists && !canMake);
         answers.add(input);

         do {
            System.out.print("Master Card Length? ");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         do {
            System.out.print("Second Card Length? ");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         do {
            System.out.print("Output Card Length? ");
            input = scanner.nextLine();
         } while (!Functions.isNumeric(input) && Integer.parseInt(input) > 0);
         answers.add(input);

         if (ex[0]) {
            do {
               System.out.print("Translate Tables? ");
               input = scanner.nextLine();
               tmpDir = new File(input);
            } while (!tmpDir.exists());
            answers.add(input);
         } else {
            answers.add(" ");
         }  if (ex[1]) {
            canMake = false;
            do {
               System.out.print("Closed File? ");
               input = scanner.nextLine();
               tmpDir = new File(input);
               exists = tmpDir.exists();
               if (exists)
                  continue;  try {
                  File file = new File(input);
                  canMake = file.createNewFile();
               } catch (IOException iOException) {}

            }
            while (!exists && !canMake);
            answers.add(input);
         } else {
            answers.add(" ");
         }
         exists = false;
         canMake = false;
         boolean exit = false;
         do {
            System.out.print("Create Answer File? ");
            input = scanner.nextLine();
            if (input.equals(" ")) {
               exit = true;
            } else {
               tmpDir = new File(input);
               exists = tmpDir.exists();
               if (!exists) {
                  try {
                     File file = new File(input);
                     canMake = file.createNewFile();
                  } catch (IOException iOException) {}
               }
            }

         } while (!exists && !canMake && !exit);
         if (!exit) {
            try {
               PrintWriter writer = new PrintWriter(new FileWriter(input));
               writer.println("BLAAAAHAHAHHAHA Date");
               writer.println("NUMBERS THAT I DONT CARE ABOUT");
               writer.println(answers.get(0));
               writer.println(answers.get(1));
               writer.println(answers.get(2));
               writer.println(answers.get(3));

               String s = answers.get(4);
               while (s.length() < 8)
                  s = " " + s;
               writer.print(s);

               s = answers.get(5);
               while (s.length() < 8)
                  s = " " + s;
               writer.print(s);

               s = answers.get(6);
               while (s.length() < 8)
                  s = " " + s;
               writer.println(s);

               writer.println(answers.get(7));
               writer.println(answers.get(8));
               writer.println(answers.get(9));

               s = answers.get(10);
               while (s.length() < 8)
                  s = " " + s;
               writer.println(s);

               s = answers.get(11);
               while (s.length() < 8)
                  s = " " + s;
               writer.println(s);

               s = answers.get(12);
               while (s.length() < 8)
                  s = " " + s;
               writer.println(s);

               if (!(answers.get(13)).equals(" ")) {
                  writer.println(answers.get(13));
               }
               if (!(answers.get(13)).equals(" ")) {
                  writer.println(answers.get(14));
               }


               writer.close();
            } catch (IOException iOException) {}
         }
      }

      return answers;
   }
}
