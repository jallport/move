//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

class TranslateTable {
   private final ArrayList<String> keys = new ArrayList();
   private final ArrayList<String> replacements = new ArrayList();
   private final ArrayList<String> closeKeys = new ArrayList();
   private final ArrayList<String> lowers = new ArrayList();
   private final ArrayList<String> uppers = new ArrayList();
   private final ArrayList<String> rNums = new ArrayList();
   public static ArrayList<String[]> closeStuff = new ArrayList<>();
   public static String name = null;

   public ArrayList<String> getKeys() {
      return this.keys;
   }

   public ArrayList<String> getReplacements() {
      return this.replacements;
   }
   public static ArrayList<String[]> getCloseStuff() {
      return closeStuff;
   }
   public static void setCloseStuff() {

   }
   public ArrayList<String> getCloseKeys() {
      return this.closeKeys;
   }

   public ArrayList<String> getLowers() {
      return this.lowers;
   }

   public ArrayList<String> getUppers() {
      return this.uppers;
   }

   public ArrayList<String> getRNums() {
      return this.rNums;
   }

   public static String getFileName() {
      return name;
   }

   static void setFileName(String name) {
      TranslateTable.name = name;
   }

   public void addTranslation(String line) {
      this.keys.add(line.substring(0, line.indexOf(44)));
      this.replacements.add(line.substring(line.indexOf(44) + 1));
   }

   public void addCloseKey(String line) {
      this.closeKeys.add(line);
   }

   public void addNumComp(String line) {
      String[] lineArr = line.split(",");
      this.lowers.add(lineArr[0]);
      this.uppers.add(lineArr[1]);
      this.rNums.add(lineArr[2]);
   }
   static void readTranslateTables(String file) {
      try {
         BufferedReader reader = new BufferedReader(new FileReader(file));
         Record.getTTables().add(new TranslateTable());

         String ttLine;
         do {
            ttLine = reader.readLine();
            if (ttLine != null && !ttLine.substring(0, 1).equals("@") && ttLine.indexOf(44) != -1 && ttLine.charAt(0) != '+' && ttLine.split(",").length == 3) {
               (Record.getTTables().get(Record.getTTables().size() - 1)).addNumComp(ttLine);
               (Record.getTTables().get(Record.getTTables().size() - 1)).addTranslation(ttLine);
            } else if (ttLine != null && !ttLine.substring(0, 1).equals("@") && ttLine.indexOf(44) != -1 && ttLine.charAt(0) != '+') {
               (Record.getTTables().get(Record.getTTables().size() - 1)).addTranslation(ttLine);
            } else if (ttLine != null && ttLine.substring(0, 1).equals("+")) {
               Record.getTTables().add(new TranslateTable());
            } else if (ttLine != null && ttLine.indexOf(44) == -1) {
               (Record.getTTables().get(Record.getTTables().size() - 1)).addCloseKey(ttLine);
            }
         } while(ttLine != null);

         reader.close();
      } catch (IOException var3) {
      }

   }
}
